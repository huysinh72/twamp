#! /bin/bash
 
image_twamp="TWAMP_image"
image_gen="Generator_image"
image_ref="Reflector_image"

#create folder image TWAMP if not exist
if [ ! -d ../"${image_twamp}" ]
then
	mkdir ../"${image_twamp}"
fi

#create folder image Generator if not exist
if [ ! -d ../"${image_twamp}"/"${image_gen}" ]
then
	mkdir ../"${image_twamp}"/"${image_gen}"
fi

#create folder image Reflector if not exist
if [ ! -d ../"${image_twamp}"/"${image_ref}" ]
then
	mkdir ../"${image_twamp}"/"${image_ref}"
fi

#create link files in folder Generator 
for file in $(ls -B ./Generator/)
do
	if [ -f "./Generator/${file}" ]
	then
		if [ ! -e ../"${image_twamp}"/"${image_gen}"/$file ]
		then
			ln -s ../../"TWAMP"/"Generator"/$file  ../"${image_twamp}"/"${image_gen}"/$file
		fi
	fi
done

#create link files in folder Reflector 
for file in $(ls -B ./Reflector/)
do
	if [ -f "./Reflector/${file}" ]
	then
		if [ ! -e ../"${image_twamp}"/"${image_ref}"/$file ]
		then
			ln -s ../../"TWAMP"/"Reflector"/$file  ../"${image_twamp}"/"${image_ref}"/$file
		fi
	fi
done

#create folder save file exec if not exist
if [ ! -d ../"${image_twamp}"/"${image_gen}"/"result_twamp" ]
then
	mkdir ../"${image_twamp}"/"${image_gen}"/"result_twamp"
fi

#run make file
make -C ../"${image_twamp}"/"${image_ref}"
make -C ../"${image_twamp}"/"${image_gen}"

echo "Install successfull............"


