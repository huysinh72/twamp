#ifndef _PARAMETER_H__
#define _PARAMETER_H__

#define MAX_PARAMETER 10
#define MAX_NAME_SERVER 12
#define MAX_ID 100

enum RequestServer{
    kClose,
    kShowAllConfig,
    kShowConfigAtId,
    kRunTwampAtId,
    kAddConfig,
    kEditConfigAtId,
    kDeleteConfigAtId,
	kReviewResultTwampAtId,
	kLockConfigAtId,
	kUnlockConfigAtId
};

typedef struct Parameter{
	int id;
	char ip_address[MAX_NAME_SERVER];
	int mode;
	int minimum_test_port_sender;
	int minimum_test_port_reflector;
	int number_test_sessions;
	int number_packets_per_test_session;
} Parameter;

typedef struct TwampResult{
	char ip_address[12];
	int sender_port;
	int reflector_port;
	int one_way_time;
	int two_way_time;
	int round_trip_time;
} TwampResult;

#endif
