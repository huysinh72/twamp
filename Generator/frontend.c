#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <dirent.h>
#include <netdb.h>
#include <sys/stat.h>
#include <time.h>
#include <errno.h>
#include "twamp.h"
#include "parameter.h"

#define MAX_THREAD 10

 /* get a string in string s from position pos to space */
char* get_string_to_space(int pos, char*s)
{
	char *ss= malloc(10*sizeof(char));
	int i, j = 0;
	for(i = pos; i < strlen(s); i++)
		if(s[i]!=' ')
			ss[j++]=s[i];
		else
			break;
	ss[j]='\0';
	return ss;
}

/* connvert string to int */
int convert_string_to_int(char *s)
{
	if(strlen(s)==0)
		return -1;
	int n= 0, i;
	for(i = 0; i < strlen(s) ; i++)
		if(s[i] >= '0' && s[i] <= '9')
			n = n*10 + (int)s[i]-48;
		else
			return -1;
	return n;
}

/* the guide for user */
void usage()
{
	printf("=============Help===============\n");
	printf("show \n");
	printf("	Show all config.\n");
	printf("show id <id> ... \n");
	printf("	Show config with id.\n");
	printf("run id <id> ... \n");
	printf("	Run to connect and check server.\n");
	printf("add ip <Ip address> mode <Mode> sender_port <test port sender> reflector_port <test port reflector> number_sessions <number of test session> number_packets <number of packets per test session> \n");
	printf("	Add connection.\n");
	printf("edit id <id> ip <Ip address> mode <Mode> sender_port <test port sender> reflector_port <test port reflector> number_sessions <number of test session> number_packets <number of packets per test session> .\n");
	printf(" 	Edit config with id. \n");
	printf("del id <id> \n");
	printf("	Delete config at position\n");
	printf("review id <id> \n");
	printf("	Show info twamp run recent at a position\n");
	printf("lock id <id> \n");
	printf("	Lock a config and other client can't use\n");
	printf("unlock id <id>");
	printf("	Unlock a config to other client can use\n");
	printf("help \n");
	printf("	Show some help for user.\n");
	printf("exit \n");
	printf("	Exit.\n");
}

/* close client socket */
void close_client(int sockfd, int request)
{
	write(sockfd, &request, sizeof(request));
}

/* printf info of config */
void printf_config(struct Parameter par)
{
	printf("***************Config with id: %d *****************\n", par.id);
	printf("Ip address server: %s\n", par.ip_address);
	printf("Modes: %d\n", par.mode);
	printf("The mininum test port sender: %d\n", par.minimum_test_port_sender);
	printf("The mininum test port reflector: %d\n", par.minimum_test_port_reflector);
	printf("The number of Test sessions: %d\n", par.number_test_sessions);
	printf("The number of Test packets per test session: %d\n", par.number_packets_per_test_session);
}

/* show all config to connect server to run twamp */
int show_all_config(int sockfd,int request)
{
	write(sockfd, &request, sizeof(request));

	int n, i, rv;
	struct Parameter par;
	rv = read(sockfd, &n, sizeof(n));
	if (rv > 0)
		for(i = 0; i< n; i++)
		{
			read(sockfd, &par, sizeof(struct Parameter));
			printf_config(par);
		}
	return rv;
}

/* get number of a number */
int number_of_number(int n)
{
	int nn = 0;

	while(1)
	{
		n = n/10;
		nn++;
		if(n==0)
			break;
	}
	return nn;
}

/* show config at id */
int show_config_at_position(int sockfd, int request, int p, char *s)
{
	int id = 0;
	int rv = 1;
	char *ss = "";
	ss=get_string_to_space(p, s);
	p = p + strlen(ss) + 1;
	if(strcmp(ss, "id")!=0)
		printf("Your command was wrong.............\n");
	else
	while(1)
	{
		if (p >= strlen(s))
			break;
		/* get id from string */
		id = convert_string_to_int(get_string_to_space(p, s));
		if(id ==-1)
		{
			printf("Your id was wrong.............\n");
			break;
		}
		p = p + number_of_number(id) + 1;
		write(sockfd, &request, sizeof(request));
		write(sockfd, &id, sizeof(id));
		struct Parameter par;
		rv = read(sockfd, &par, sizeof(struct Parameter));
		if(par.id != -1 && rv > 0)
			printf_config(par);
		else
		{
			printf("Your id not exist or deleted.............\n");
			break;
		}
	}
	return rv;
}

/* run twamp with id of config */
int run_twamp(int sockfd,int request, int p, char *s)
{
	int id, rv = 1, n = 0;
	int arr_id[10];
	char *ss = "";
	ss=get_string_to_space(p, s);
	p = p + strlen(ss) + 1;
	if(strcmp(ss, "id")!=0)
		printf("Your command was wrong.............\n");
	else
	{
		while(1)
		{
			if (p >= strlen(s))
				break;
			id = convert_string_to_int(get_string_to_space(p, s));
			if(id ==-1)
			{
				printf("Your id was wrong.............\n");
				return;
			}
			p = p + number_of_number(id) + 1;

			int res;
			write(sockfd, &request, sizeof(request));
			write(sockfd, &id, sizeof(int));
			rv = read(sockfd, &res, sizeof(int)); // read return result to know run success
			if(res > 0) // if run success, show result of twamp such as: one-way delay time, two-way delay time in file infoTwamp<id>.txt
			{
				printf("Run twamp with id %d successfull..........\n", id);
			}
			else
			{
				printf("Run fail...Maybe cause is config not exist or was locked.........\n");
			}
		}
	}
	return rv;

}

/* get parameters of config from string */
struct Parameter get_par_from_string(int p, char *s)
{
	/* init value */
	struct Parameter par;
	strcpy(par.ip_address, "");
	par.mode = -1;
	par.minimum_test_port_reflector = -1;
	par.minimum_test_port_sender = -1;
	par.number_packets_per_test_session = -1;
	par.number_test_sessions = -1;

	char *ss = "";
	while(1)
	{
		ss = get_string_to_space(p, s);
		p = p + strlen(ss) + 1;
		if(strcmp(ss, "ip")==0)
		{
			/* get ip address from string s */
			strcpy(par.ip_address,get_string_to_space(p, s));
			if (strlen(par.ip_address)==0)
			{
				printf("Your ip_address was wrong....\n");
				return par;
			}
			p = p + strlen(par.ip_address) + 1;
		}else
		if(strcmp(ss, "mode")==0)
		{
			/* get mode from string s  */
			par.mode=convert_string_to_int(get_string_to_space(p, s));
			if(par.mode == -1 || number_of_number(par.mode) > 1)
			{
				printf("Your mode was wrong (it's a number into 1, 2, 4, 8)....\n");
				return par;
			}
			p = p + 2;
		}else
		if(strcmp(ss, "sender_port")==0)
		{
			/* get minimum_test port sender from string s */
			par.minimum_test_port_sender=convert_string_to_int(get_string_to_space(p, s));
			if(par.minimum_test_port_sender == -1 || number_of_number(par.minimum_test_port_sender) < 4)
			{
				printf("Your port sender was wrong (it's a number and > 1000)....\n");
				return par;
			}
			p = p + number_of_number(par.minimum_test_port_sender) + 1;
		}else
		if(strcmp(ss, "reflector_port")==0)
		{
			/* get minimum test port reflector from string s */
			par.minimum_test_port_reflector=convert_string_to_int(get_string_to_space(p, s));
			if(par.minimum_test_port_reflector == -1 || number_of_number(par.minimum_test_port_reflector) < 4)
			{
				printf("Your port reflector was wrong (it's a number and > 1000)....\n");
				return par;
			}
			p = p + number_of_number(par.minimum_test_port_reflector) + 1;
		}else
		if(strcmp(ss, "number_sessions")==0)
		{
			/* get number of test session from string s */
			par.number_test_sessions=convert_string_to_int(get_string_to_space(p, s));
			if(par.number_test_sessions == -1)
			{
				printf("Your number of test sessions was wrong (it's a number)....\n");
				return par;
			}
			p = p + number_of_number(par.number_test_sessions) + 1;
		}else
		if(strcmp(ss, "number_packets")==0)
		{
			/* get number of packets per test session from string s */
			par.number_packets_per_test_session=convert_string_to_int(get_string_to_space(p, s));
			if(par.number_packets_per_test_session == -1)
			{
				printf("Your number of packets per test sessions wrong (it's a number)....\n");
				return par;
			}
			p = p + number_of_number(par.number_packets_per_test_session) + 1;
		}else
		if(strlen(ss) > 0)
		{
			printf("You enter name parameter wrong...............\n");
			break;
		}
		else
			break;
	}
	return par;
}

/* check parameter have enough */
int check_enough_par(struct Parameter par)
{
	if(strlen(par.ip_address)==0)
		return -1;
	if(par.mode < 0)
		return -1;
	if(par.minimum_test_port_reflector < 0)
		return -1;
	if(par.minimum_test_port_sender < 0)
		return -1;
	if(par.number_packets_per_test_session < 0)
		return -1;
	if(par.number_test_sessions < 0)
		return -1;
	return 1;
}

/* add config to list config */
int add_config(int sockfd,int request, int p, char *s)
{
	int rv = 1;
	struct Parameter par;
	par = get_par_from_string(p, s);
	if(check_enough_par(par) > 0)
	{
		write(sockfd, &request, sizeof(request));
		write(sockfd, &par, sizeof(struct Parameter));
		int res;
		rv = read(sockfd, &res, 4);
		if(rv > 0)
		{
			printf("Add config successful have id: %d............\n", res);
		}
		else
			printf("Add fail..............\n");
	}
	else
		printf("You enter not enough to parameter..........\n");
	return rv;
}

/* edit config at id */
int edit_config(int sockfd,int request, int p, char *s)
{

	int id = 0, rv = 1;
	char *ss = "";
	ss=get_string_to_space(p, s);
	p = p + strlen(ss) + 1;
	if(strcmp(ss, "id")!=0)
		printf("Your command was wrong.............\n");
	else
	{
		/* read id from string s */
		id = convert_string_to_int(get_string_to_space(p, s));
		if(id ==-1)
		{
			printf("Your id was wrong.............\n");
			return;
		}
		p = p + number_of_number(id) + 1;

		struct Parameter par;
		par = get_par_from_string(p, s);

		write(sockfd, &request, sizeof(request));
		write(sockfd, &id, sizeof(int));
		write(sockfd, &par, sizeof(struct Parameter));
		int res = 1;
		rv = read(sockfd, &res, sizeof(int));
		if(res > 0)
			printf("Edit successfull ..........\n");
		else
			printf("Edit fail... maybe cause not exit id or another client clocked................\n");
	}
	return rv;
}

/* delete config at a lot id */
int delete_config(int sockfd, int request, int p, char*s)
{
	int id = 0, rv = 1;
	char *ss = "";
	ss=get_string_to_space(p, s);
	p = p + strlen(ss) + 1;
	if(strcmp(ss, "id")!=0)
		printf("Your command was wrong.............\n");
	else
	while(1)
	{
		if (p >= strlen(s))
			break;
		/* read id from string */
		id = convert_string_to_int(get_string_to_space(p, s));
		if(id ==-1)
		{
			printf("Your id was wrong.............\n");
			break;
		}
		p = p + number_of_number(id) + 1;

		write(sockfd, &request, sizeof(request));
		write(sockfd, &id, sizeof(id));
		int res = 1;
		rv = read(sockfd, &res, sizeof(int));
		if(res > 0)
			printf("Delete successfull at id: %d.............\n", id);
		else
			printf("Delete id %d fail... maybe cause id doesn't exist or another client locked......\n", id);
	}
	return rv;
}

/* review result of twamp at id */
int review(int sockfd, int request, int p, char*s)
{
	int id = 0, rv = 1;
	char *ss = "";
	ss=get_string_to_space(p, s);
	p = p + strlen(ss) + 1;
	if(strcmp(ss, "id")!=0)
		printf("Your command was wrong.............\n");
	else
	while(1)
	{
		if (p >= strlen(s))
			break;
		id = convert_string_to_int(get_string_to_space(p, s));
		if(id ==-1)
		{
			printf("Your id was wrong.............\n");
			break;
		}
		p = p + number_of_number(id) + 1;
		write(sockfd, &request, sizeof(request));
		write(sockfd, &id, sizeof(int));
		int res;
		rv = read(sockfd, &res, sizeof(res));
		if (res == -1)
			printf("Your id %d doesn't exist,  deleted, or yet run .....\n", id);
		else
		if(res == 1)
		{
			struct TwampResult resTwamp;
			rv = read(sockfd, &resTwamp, sizeof(struct TwampResult));
			fprintf(stdout, "Ip address server twamp: ");
			fprintf(stdout, "%s\n", resTwamp.ip_address);
			fprintf(stdout, "Sender port: ");
			fprintf(stdout, "%d\n", resTwamp.sender_port);
			fprintf(stdout, "Reflector port: ");
			fprintf(stdout, "%d\n", resTwamp.reflector_port);
			fprintf(stdout, "Round trip delay time: ");
			fprintf(stdout, "%d\n", resTwamp.round_trip_time);
			fprintf(stdout, "One way delay time: ");
			fprintf(stdout, "%d\n", resTwamp.one_way_time);
			fprintf(stdout, "Two way delay time: ");
			fprintf(stdout, "%d\n", resTwamp.two_way_time);

		}
		else
		if(res == 0)
			printf("Twamp at id %d is running......\n", id);
		else
		if (res == -2)
		{
			printf("Run twamp at id %d was error..........\n", id);
			int size;
			char ss[100];
			while (1)
			{
				read(sockfd, &size, sizeof(size));
				if(size == 0)
					break;
				read(sockfd, &ss, sizeof(ss));
				printf("%s", ss);
			}
		}
	}
	return rv;
}

/* lock config at id */
int lock_config(int sockfd, int request, int p, char*s)
{
	int id = 0, rv = 1;
	char *ss = "";
	ss=get_string_to_space(p, s);
	p = p + strlen(ss) + 1;
	if(strcmp(ss, "id")!=0)
		printf("Your command was wrong.............\n");
	else
	while(1)
	{
		if (p >= strlen(s))
			break;
		id = convert_string_to_int(get_string_to_space(p, s));
		if(id ==-1)
		{
			printf("Your id was wrong.............\n");
			break;
		}
		p = p + number_of_number(id) + 1;

		write(sockfd, &request, sizeof(request));
		write(sockfd, &id, sizeof(id));
		int res = 0;
		rv = read(sockfd, &res, sizeof(int));
		if(res > 0)
			printf("Lock successfull at id: %d.............\n", id);
		else
			printf("Lock fail... maybe cause id doesn't exist or another client locked......\n");
	}
	return rv;
}

/* unlock config at id */
int unlock_config(int sockfd, int request, int p, char*s)
{
	int id = 0, rv = 1;
	char *ss = "";
	ss=get_string_to_space(p, s);
	p = p + strlen(ss) + 1;
	if(strcmp(ss, "id")!=0)
		printf("Your command was wrong.............\n");
	else
	while(1)
	{
		if (p >= strlen(s))
			break;
		id = convert_string_to_int(get_string_to_space(p, s));
		if(id ==-1)
		{
			printf("Your id was wrong.............\n");
			break;
		}
		p = p + number_of_number(id) + 1;

		write(sockfd, &request, sizeof(request));
		write(sockfd, &id, sizeof(id));
		int res = 0;
		rv = read(sockfd, &res, sizeof(int));
		if(res > 0)
			printf("Unlock successfull at id: %d.............\n", id);
		else
			printf("Unlock fail... maybe cause id doesn't exist or another client locked......\n");
	}
	return rv;
}

int main()
{
	int sockfd;
	int len;
	struct sockaddr_in address;

	int result;
	char ch='A';
	/* create socket follow Internet standard
	  use type communication TCP and protocol apply for type socket is get default protocol */

	sockfd=socket(AF_INET, SOCK_STREAM, 0);

	address.sin_family=AF_INET;
	address.sin_addr.s_addr=inet_addr("127.0.0.1");
	address.sin_port=htons(9995);

	len=sizeof(address);
	result=connect(sockfd, (struct sockaddr*)&address, len);

	if (result ==-1){
		perror("opps: client problem");
		return 1;
	}

	usage();
	printf("===========================...===========================\n");

	char *command = NULL;
	char *request = NULL;
	size_t size;
	int rv, cc = 1;

	while(1)
	{
		printf("Your choose:");
		getline(&command, &size, stdin);
		int len = strlen(command);
		command[len-1]=' ';
		request = get_string_to_space(0, command);
		rv = 1;

		if (strcmp(request, "exit")==0)
		{
			/* close frontend */
			close_client(sockfd, kClose);
			break;

		}else
		if(strcmp(request, "show")==0)
		{
			if(strlen(command) == strlen("show")+1)
				/* show all config */
				rv = show_all_config(sockfd, kShowAllConfig);
			else
				/* show config at at id */
				rv = show_config_at_position(sockfd, kShowConfigAtId, strlen("show")+1, command);

		}else
		if(strcmp(request, "run")==0)
		{
			if(strlen(command) > strlen("run")+1)
				/* run twamp at id*/
				rv = run_twamp(sockfd, kRunTwampAtId, strlen("run")+1, command);
			else
				printf("Your command was wrong...\n");
		}else
		if(strcmp(request, "add")==0)
		{
			if(strlen(command) > strlen("add")+1)
				/* add config */
				rv = add_config(sockfd, kAddConfig, strlen("add")+1, command);
			else
				printf("Your command was wrong...\n");

		}else
		if(strcmp(request, "edit")==0)
		{
			if(strlen(command) > strlen("edit")+1)
				/* edit config at id */
				rv = edit_config(sockfd, kEditConfigAtId, strlen("edit")+1, command);
			else
				printf("Your command was wrong...\n");

		}else
		if(strcmp(request, "del")==0)
		{
			if(strlen(command) > strlen("del")+1)
				/* delete config at id */
				rv = delete_config(sockfd, kDeleteConfigAtId, strlen("del")+1, command);
		}else
		if(strcmp(request, "help")==0)
		{
			usage();
		}else
		if(strcmp(request, "review")==0)
		{
			if(strlen(command) > strlen("review")+1)
			{
				/* review result twamp at id */
				rv = review(sockfd, kReviewResultTwampAtId, strlen("review")+1, command);
			}
			else
				printf("Your command was wrong...\n");
		}else
		if(strcmp(request, "lock")==0)
		{
			if(strlen(command) > strlen("lock")+1)
			{
				/* lock config at id*/
				rv = lock_config(sockfd, kLockConfigAtId, strlen("lock")+1, command);
			}
			else
				printf("Your command was wrong...\n");

		}else
		if(strcmp(request, "unlock")==0)
		{
			if(strlen(command) > strlen("unlock")+1)
			{
				/* unlock a config at id */
				rv = unlock_config(sockfd, kUnlockConfigAtId, strlen("unlock")+1, command);
			}
			else
				printf("Your command was wrong...\n");
		}
		else
		{
			printf("Your choose doesn't exist............\n");
		}

		if (rv < 1)
		{
			printf("Backend disconnected.............\n");
			break;
		}
		printf("===========================...===========================\n");
	}
	close(sockfd);
	return 0;
}

