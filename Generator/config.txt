9
[Twamp-1]
Id: 1
Ip_address: 127.0.0.1
Modes: 1
Minimum_port_sender: 3000
Minimum_port_reflector: 4000
Number_of_test_sessions: 1
Number_of_packets_per_test_session: 1

[Twamp-2]
Id: 2
Ip_address: 127.0.1.1
Modes: 1
Minimum_port_sender: 5500
Minimum_port_reflector: 6000
Number_of_test_sessions: 1
Number_of_packets_per_test_session: 1

[Twamp-3]
Id: 3
Ip_address: 127.0.0.1
Modes: 1
Minimum_port_sender: 2000
Minimum_port_reflector: 3000
Number_of_test_sessions: 1
Number_of_packets_per_test_session: 1

[Twamp-4]
Id: 4
Ip_address: 127.0.0.1
Modes: 1
Minimum_port_sender: 2000
Minimum_port_reflector: 3000
Number_of_test_sessions: 1
Number_of_packets_per_test_session: 1

[Twamp-5]
Id: 5
Ip_address: 127.0.0.1
Modes: 1
Minimum_port_sender: 2000
Minimum_port_reflector: 3000
Number_of_test_sessions: 1
Number_of_packets_per_test_session: 1

[Twamp-6]
Id: 6
Ip_address: 127.0.0.1
Modes: 1
Minimum_port_sender: 8000
Minimum_port_reflector: 9000
Number_of_test_sessions: 1
Number_of_packets_per_test_session: 1

[Twamp-7]
Id: 7
Ip_address: 127.0.0.1
Modes: 1
Minimum_port_sender: 2000
Minimum_port_reflector: 3000
Number_of_test_sessions: 1
Number_of_packets_per_test_session: 1

[Twamp-8]
Id: 8
Ip_address: 127.0.0.1
Modes: 1
Minimum_port_sender: 2000
Minimum_port_reflector: 3000
Number_of_test_sessions: 1
Number_of_packets_per_test_session: 1

[Twamp-9]
Id: 9
Ip_address: 127.0.0.1
Modes: 1
Minimum_port_sender: 2500
Minimum_port_reflector: 3500
Number_of_test_sessions: 1
Number_of_packets_per_test_session: 1
