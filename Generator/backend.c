#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <dirent.h>
#include <netdb.h>
#include <sys/stat.h>
#include <time.h>
#include <pthread.h>
#include <errno.h>
#include "twamp.h"
#include "parameter.h"

/* file name save config */
#define NAME_FILE_CONFIG "config.txt"
/* file name save result when run twamp */
#define NAME_FILE_TWAMP "./result_twamp/infoTwamp"
#define NAME_FILE_RESULT_RUN "./result_twamp/infoRunTwamp"
#define MAX_CLIENT 10
static int arr_flags[MAX_ID]; // 0 running, 1 run done
int arr_clientfd_id[MAX_CLIENT*10][MAX_ID];
int arr_id[MAX_ID];

/* init array */
void init()
{
	int i, j;
	for(i = 0; i < 100; i++)
		for(j = 0; j < MAX_ID; j++)
			arr_clientfd_id[MAX_CLIENT*10][j] = 0;
	for(i = 0; i < MAX_ID; i++)
		arr_id[i] = 0;
}

/* read data from file config */
void read_data(int *n, struct Parameter *arr_par)
{
	FILE* f_read_config;
	f_read_config=fopen(NAME_FILE_CONFIG, "r");
	
	int i;
	char s[30];
	fscanf(f_read_config, "%d", &(*n));
	for(i = 0; i < *n; i++)
	{
		fscanf(f_read_config, "%s", s);
		fscanf(f_read_config, "%s", s);
		fscanf(f_read_config, "%d", &arr_par[i].id);
		fscanf(f_read_config, "%s", s);
		fscanf(f_read_config, "%s", arr_par[i].ip_address);
		fscanf(f_read_config, "%s", s);
		fscanf(f_read_config, "%d", &arr_par[i].mode);
		fscanf(f_read_config, "%s", s);
		fscanf(f_read_config, "%d", &arr_par[i].minimum_test_port_sender);
		fscanf(f_read_config, "%s", s);
		fscanf(f_read_config, "%d", &arr_par[i].minimum_test_port_reflector);
		fscanf(f_read_config, "%s", s);
		fscanf(f_read_config, "%d", &arr_par[i].number_test_sessions);
		fscanf(f_read_config, "%s", s);
		fscanf(f_read_config, "%d", &arr_par[i].number_packets_per_test_session);
		//if(i < *n-1)
			//fscanf(f_read_config, "%s", s);
	}
	fclose(f_read_config);
} 

/* write data cofig to file */
void write_data(int n, struct Parameter *arr_par)
{
	FILE* f_write_config;
	f_write_config=fopen(NAME_FILE_CONFIG, "w");
	
	int i;
	fprintf(f_write_config, "%d\n", n);
	for(i = 0; i < n; i++)
	{
		fprintf(f_write_config, "[Twamp-%d]\n", arr_par[i].id);
		fprintf(f_write_config, "Id: %d\n", arr_par[i].id);
		fprintf(f_write_config, "Ip_address: %s\n", arr_par[i].ip_address);
		fprintf(f_write_config, "Modes: %d\n", arr_par[i].mode);
		fprintf(f_write_config, "Minimum_port_sender: %d\n", arr_par[i].minimum_test_port_sender);
		fprintf(f_write_config, "Minimum_port_reflector: %d\n", arr_par[i].minimum_test_port_reflector);
		fprintf(f_write_config, "Number_of_test_sessions: %d\n", arr_par[i].number_test_sessions);
		fprintf(f_write_config, "Number_of_packets_per_test_session: %d\n", arr_par[i].number_packets_per_test_session);
		if (i < n-1)
			fprintf(f_write_config, "\n");
	}
	fclose(f_write_config);
} 

/* review result of twamp at id */
void send_result_twamp_to_frontend(int client_sockfd, int id, int rv)
{
	FILE* f_read = NULL;
	char filename[100] = "";
	char s[30];
	struct TwampResult res;

	/* get file name follow id */
	sprintf(filename, "%s%d.txt", NAME_FILE_TWAMP, id);
	f_read = fopen(filename, "r");
	if(f_read == NULL && rv > 0)
		rv = -1;
	else
		if(f_read != NULL && rv != 0)
		{
			res.sender_port = -1;
			fscanf(f_read, "%s", s);
			fscanf(f_read, "%s", res.ip_address);
			fscanf(f_read, "%s", s);
			fscanf(f_read, "%d", &res.sender_port);
			fscanf(f_read, "%s", s);
			fscanf(f_read, "%d", &res.reflector_port);
			fscanf(f_read, "%s", s);
			fscanf(f_read, "%d", &res.round_trip_time);
			fscanf(f_read, "%s", s);
			fscanf(f_read, "%d", &res.one_way_time);
			fscanf(f_read, "%s", s);
			fscanf(f_read, "%d", &res.two_way_time);
			if(res.sender_port == -1)
				rv = -2;
		}

	write(client_sockfd, &rv, sizeof(int));
	if (rv > 0)
	{
		write(client_sockfd, &res, sizeof(struct TwampResult));
		fclose(f_read);
	}
	else
	if(rv == -2)
	{
		if (f_read != NULL)
			fclose(f_read);

		sprintf(filename, "%s%d.txt", NAME_FILE_RESULT_RUN, id);
		f_read = fopen(filename, "r");

		size_t size;
		char * buf=NULL;
		while(getline(&buf,&size,f_read)!=EOF){ // read to finish file
				write(client_sockfd, &size, sizeof(int));
				write(client_sockfd, buf, 100);
			}
		size = 0;
		write(client_sockfd, &size, sizeof(int));
		fclose(f_read);
	}
}

/* get config by id */
int get_config_with_id(int id, int n, struct Parameter *arr_par)
{
	int i;
	for(i = 0; i < n; i++)
		if(arr_par[i].id == id)
			return i;
	return -1;
}

/* params for fuction of thread */
struct ReadThreadParams
{
	struct Parameter par;
	int id;
};

/*function for thread */
void *create_thread_to_run_twamp(void *data)
{
	struct ReadThreadParams *parf = data;
	arr_flags[parf->id] = run(parf->par, parf->id);
}

/* create thread run twamp */
void run_twamp(int id, struct Parameter par)
{
	int thr_id;
	pthread_t pthread;
	struct ReadThreadParams *parf = malloc(sizeof(struct ReadThreadParams));

	parf->par = par;
	parf->id = id;
	thr_id = pthread_create(&pthread, NULL, create_thread_to_run_twamp, parf);
	if(thr_id != 0)
		perror("Thread created error:");
}

/* unlock when client loss connect */
void unlock(int client_sockfd)
{
	int i;
	for(i = 0; i < MAX_ID; i++)
		if(arr_clientfd_id[client_sockfd][i])
		{
			arr_id[i] = 0;
			arr_clientfd_id[client_sockfd][i] = 0;
		}
}

/* function to handling a request is sent to*/
int handling_request(int client_sockfd, int n, struct Parameter *arr_par)
{
	int id_request;
	int i;
	/* read request from frontend */
	int rv= read(client_sockfd, &id_request, sizeof(int));
	if (rv <= 0)
		return -1;

	/* Client closed connect */
	if(id_request==kClose)
	{
		printf("Client have file description %d closed\n", client_sockfd);
		unlock(client_sockfd);
		close(client_sockfd);
		return 0;
	}

	switch (id_request){
		/* Show all config of connection */
		case kShowAllConfig:
			printf("Client with file description %d require show all config\n", client_sockfd);
			write(client_sockfd, &n, 4);
			for(i = 0; i < n; i++)
				write(client_sockfd, &arr_par[i], sizeof(struct Parameter));

			break;
		/* Show config at id */
		case kShowConfigAtId:
		{
			int id, pos;
			read(client_sockfd, &id, 4);
			pos = get_config_with_id(id, n, arr_par);
			if(pos >= 0)
			{
				printf("Client with file description %d require show config with id: %d\n",client_sockfd, id);
				write(client_sockfd, &arr_par[pos], sizeof(struct Parameter));
			}
			else
			{
				struct Parameter par;
				par.id = -1;
				write(client_sockfd, &par, sizeof(struct Parameter));
			}

			break;
		}
		/* Run twamp of connection at id */
		case kRunTwampAtId:
		{
			int i, pos, rv = 1, id;
			read(client_sockfd, &id, sizeof(int));
			pos = get_config_with_id(id, n, arr_par);
			printf("Client with file description %d require run twamp with id %d\n",client_sockfd, id);
				if(pos < 0)
					rv = -1;
				else
				{
					if(!arr_id[id] || arr_clientfd_id[client_sockfd][id])
					{
						arr_flags[id] = 0;
						run_twamp(id, arr_par[pos]);
					}
					else
						rv = 0;
				}

			write(client_sockfd, &rv, sizeof(int));
			break;
		}
		/* Add a config */
		case kAddConfig:
		{
			int rv = -1, m;
			struct Parameter par;
			read(client_sockfd, &par, sizeof(struct Parameter));
			printf("Client with file description %d require add config have Ip address: %s\n", client_sockfd, par.ip_address);

			for(i = 0; i < n; i++)
				if(arr_par[i].id != i+1)
					break;
			if(i == n-1 && arr_par[i].id == i+1)
				i++;
			m = i;
			for(i = n; i > m; i--)
				arr_par[i] = arr_par[i-1];

			par.id = m+1;
			arr_par[m]=par;
			rv = m + 1;
			n++;
			write(client_sockfd, &rv, sizeof(int));
			write_data(n, arr_par);
			return 4;
		}
		/* Edit a config at id */
		case kEditConfigAtId:
		{
			struct Parameter par;
			int pos, id, rv = -1;
			read(client_sockfd, &id, sizeof(int));
			pos = get_config_with_id(id, n, arr_par);
			if(pos >= 0)
			{
				printf("Client with file description %d require fix config with id: %d\n", client_sockfd, id);
				rv = read(client_sockfd, &par, sizeof(struct Parameter));
				if(!arr_id[id] || arr_clientfd_id[client_sockfd][id])
				{
					if(strlen(par.ip_address) > 0)
						strcpy(arr_par[pos].ip_address,par.ip_address);
					if(par.mode > 0)
						arr_par[pos].mode = par.mode;
					if(par.minimum_test_port_sender > 0)
						arr_par[pos].minimum_test_port_sender = par.minimum_test_port_sender;
					if(par.minimum_test_port_reflector > 0)
						arr_par[pos].minimum_test_port_reflector = par.minimum_test_port_reflector;
					if(par.number_test_sessions > 0)
						arr_par[pos].number_test_sessions = par.number_test_sessions;
					if(par.number_packets_per_test_session > 0)
						arr_par[pos].number_packets_per_test_session = par.number_packets_per_test_session;
					rv = 1;
					write_data(n, arr_par);
				}
				else
					rv = 0;
			}
			write(client_sockfd, &rv, 4);
			break;

		}
		/* Delete config at id */
		case kDeleteConfigAtId:
		{
			int pos, i, rv = -1, id;
			read(client_sockfd, &id, sizeof(int));
			pos = get_config_with_id(id, n, arr_par);
			if(pos >= 0)
			{
				printf("Client with file description %d require delete config with id: %d\n", client_sockfd, id);
				if(!arr_id[id] || arr_clientfd_id[client_sockfd][id])
				{
					for(i = pos; i < n-1; i++)
						arr_par[i] = arr_par[i+1];
					n--;
					rv = 1;
					write_data(n, arr_par);

					char filename[100] = "";
					/* get file name follow id */
					sprintf(filename, "%s%d.txt", NAME_FILE_TWAMP, id);
					remove(filename);
				}
				else
					rv = 0;
			}
			write(client_sockfd, &rv, sizeof(int));
			if (rv ==1 )return 6;
			break;
		}
		/* Review result twamp at id */
		case kReviewResultTwampAtId:
		{
			int pos, i, rv = -1, id = 0;
			read(client_sockfd, &id, sizeof(int));
			pos = get_config_with_id(id, n, arr_par);
			if(pos >= 0)
			{
				printf("Client with file description %d require review config with id: %d\n", client_sockfd, id);
				rv = arr_flags[id];
			}
			send_result_twamp_to_frontend(client_sockfd, id, rv);
			break;
		}
		/* Lock config at id */
		case kLockConfigAtId:
		{
			int pos, i, rv = -1, id;
			read(client_sockfd, &id, sizeof(int));
			pos = get_config_with_id(id, n, arr_par);
			if(pos >= 0)
			{
				printf("Client with file description %d require lock config with id: %d\n", client_sockfd, id);
				rv = 1;
				if (!arr_id[id])
				{
					arr_clientfd_id[client_sockfd][id] = 1;
					arr_id[id] = 1;
				}
				else
					rv = 0;
			}
			write(client_sockfd, &rv, sizeof(int));
			break;
		}
		/* Unlock config at id */
		case kUnlockConfigAtId:
		{
			int pos, i, rv = -1, id;
			read(client_sockfd, &id, sizeof(int));
			pos = get_config_with_id(id, n, arr_par);
			if(pos >= 0)
			{
				printf("Client with file description %d require unlock config with id: %d\n", client_sockfd, id);
				rv = 1;
				if (arr_id[id] && arr_clientfd_id[client_sockfd][id])
				{
					arr_clientfd_id[client_sockfd][id] = 0;
					arr_id[id] = 0;
				}
				else
					rv = 0;
			}
			write(client_sockfd, &rv, sizeof(int));
			break;
		}
	}
	return 1;
}


int main()
{
	init();
	int n, i, nclient = 0;
	int arr_client[MAX_CLIENT];
	struct Parameter arr_par[MAX_PARAMETER];	

	/* read list config from file */
	read_data(&n, arr_par);
	
	for(i = 0; i < MAX_ID; i++)
		arr_flags[i] = 1;
	
	int server_sockfd, client_sockfd;
	int server_len, client_len;
	
	struct sockaddr_in server_address;
	struct sockaddr_in client_address;
	
	/* create socket follow Internet standard
		  and use type communication TCP and protocol apply for type socket is get default protocol */
	server_sockfd=socket(AF_INET, SOCK_STREAM, 0);

	server_address.sin_family=AF_INET;
	server_address.sin_addr.s_addr=htonl(INADDR_ANY);
	server_address.sin_port=htons(9995);
	
	server_len=sizeof(server_address);
	client_len=sizeof(client_sockfd);
	bind(server_sockfd, (struct sockaddr*)&server_address, server_len);
	
	listen(server_sockfd, 5);
	
	fd_set readfds, testfds;
	int fd_max = 0, result;
	FD_ZERO(&readfds);

	FD_SET(server_sockfd, &readfds);
	fd_max = server_sockfd;

	while(1){
		int fd;
		testfds = readfds;
		printf("Backend is waiting.....\n");
		result = select(fd_max+1, &testfds, NULL, NULL, NULL);
		if(result < 1)
		{
			perror("Error");
			return 1;
		}

		if(FD_ISSET(server_sockfd, &testfds))
		{
			client_sockfd=accept(server_sockfd, (struct sockaddr*)&client_address, &client_len);
			if(client_sockfd > 0)
			{
				FD_SET(client_sockfd, &readfds);
				arr_client[nclient++] = client_sockfd;
				printf("Have a new connection with file description: %d\n", client_sockfd);
				if(client_sockfd > fd_max)
					fd_max= client_sockfd;
			}
			else
				perror("Error in accept..");
		}

		int rv;
		for(fd=0; fd < fd_max+1; fd++)
		if(FD_ISSET(fd, &testfds) && fd != server_sockfd)
		{
			rv = handling_request(fd, n, arr_par);
			if(rv == 0)
				FD_CLR(fd, &readfds);
			else
			if(rv == 4) // if add config then increase n
				n++;
			else
			if(rv == 6) // if delete config then decrease n
				n--;
			else
			if(rv == -1)
			{
				printf("Error connection at socket description %d....\n", fd);
				FD_CLR(fd, &readfds);
			}
		}


	}
	
	return 0;
}
