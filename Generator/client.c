
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <dirent.h>
#include <netdb.h>
#include <sys/stat.h>
#include <time.h>
#include <errno.h>
#include "twamp.h"
#include "parameter.h"

#define PORTBASE_SEND    30000
#define PORTBASE_RECV    20000
#define TEST_SESSIONS    1
#define TEST_MESSAGES    1
#define TIMEOUT          5 /* SECONDS */
#define NAME_FILE_RESULT_TWAMP		 "./result_twamp/infoTwamp"
#define NAME_FILE_RESULT_RUN		 "./result_twamp/infoRunTwamp"


struct twamp_test_info {
    int testfd;
    int testport;
    uint16_t port;
};


static enum Mode authmode = kModeUnauthenticated;
static int port_send = PORTBASE_SEND;
static int port_recv = PORTBASE_RECV;

static uint16_t test_sessions_no = TEST_SESSIONS;
static uint32_t test_sessions_msg = TEST_MESSAGES;
static uint16_t active_sessions = 0;

/* init data */
int init_config(struct Parameter par)
{
	port_send = par.minimum_test_port_sender;
	port_recv = par.minimum_test_port_reflector;
	test_sessions_no = par.number_test_sessions;
	test_sessions_msg = par.number_packets_per_test_session;
	return 1;
}


/* This function sends StopSessions to stop all active Test sessions */
static int send_stop_session(int socket, int accept, int sessions)
{
    StopSessions stop;
    memset(&stop, 0, sizeof(stop));
    stop.Type = kStopSessions;
    stop.Accept = accept;
    stop.SessionsNo = sessions;
    return send(socket, &stop, sizeof(stop), 0);
}

static int send_start_sessions(int socket)
{
    StartSessions start;
    memset(&start, 0, sizeof(start));
    start.Type = kStartSessions;
    return send(socket, &start, sizeof(start), 0);
}

static char *get_accept_str(int code)
{
    switch (code) {
    case kOK:
        return "OK.";
    case kFailure:
        return "Failure, reason unspecified.";
    case kInternalError:
        return "Internal error.";
    case kAspectNotSupported:
        return "Some aspect of request is not supported.";
    case kPermanentResourceLimitation:
        return "Cannot perform request due to permanent resource limitations.";
    case kTemporaryResourceLimitation:
        return "Cannot perform request due to temporary resource limitations.";
    default:
        return "Undefined failure";
    }
}


int run(struct Parameter par, int id)
{
	FILE*f_write = NULL;
	FILE*f_write_run = NULL;
	char filename[100] = "";
	sprintf(filename, "%s%d.txt", NAME_FILE_RESULT_TWAMP, id);

	f_write = fopen(filename, "w");
	sprintf(filename, "%s%d.txt", NAME_FILE_RESULT_RUN, id);
	f_write_run = fopen(filename, "w");

    srand(time(NULL));

    struct sockaddr_in serv_addr;
    init_config(par);// init config for client

    /* Create server socket connection for the TWAMP-Control session */
    int servfd = socket(AF_INET, SOCK_STREAM, 0);
    if (servfd < 0) {
        perror("Error opening socket");
        fprintf(f_write_run, "Error opening socket\n");
        fclose(f_write);
        fclose(f_write_run);
        return -2;
    }

    memset(&serv_addr, 0, sizeof(serv_addr));
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_addr.s_addr = inet_addr(par.ip_address);
    serv_addr.sin_port = htons(SERVER_PORT);

    fprintf(f_write, "Ip_address_server: %s\n", par.ip_address);
    if (connect(servfd, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0) {
        perror("Error connecting");
        fprintf(f_write_run, "Error connect to server \n");
        fclose(f_write);
        fclose(f_write_run);
        return -2;
    }

    /* TWAMP-Control change of messages after TCP connection is established */

    /* Receive Server Greeting and check Modes */
    ServerGreeting greet;
    memset(&greet, 0, sizeof(greet));
    int rv = recv(servfd, &greet, sizeof(greet), 0);
    if (rv <= 0) {
        close(servfd);
        perror("Error receiving Server Greeting");
        fprintf(f_write_run, "Error receiving Server Greeting");
        fclose(f_write);
        fclose(f_write_run);
        return -2;
    }
    if (greet.Modes == 0) {
        close(servfd);
        fprintf(f_write_run, "The server does not support any usable Mode\n");
        fclose(f_write);
        fclose(f_write_run);
        return -2;
    }
    fprintf(f_write_run, "Received ServerGreeting.\n");

    /* Compute SetUpResponse */
    fprintf(f_write_run, "Sending SetUpResponse...\n");
    SetUpResponse resp;
    memset(&resp, 0, sizeof(resp));
    resp.Mode = greet.Modes & authmode;
    rv = send(servfd, &resp, sizeof(resp), 0);
    if (rv <= 0) {
        close(servfd);
        perror("Error sending Greeting Response");
        fprintf(f_write_run, "Error sending Greeting Response\n");
        fclose(f_write);
        fclose(f_write_run);
        return -2;
    }

    /* Receive ServerStart message */
    ServerStart start;
    memset(&start, 0, sizeof(start));
    rv = recv(servfd, &start, sizeof(start), 0);
    if (rv <= 0) {
        close(servfd);
        perror("Error Receiving Server Start");
        fprintf(f_write_run, "Error Receiving Server Start\n");
        fclose(f_write);
        fclose(f_write_run);
        return -2;
    }
    /* If Server did not accept our request */
    if (start.Accept != kOK) {
        close(servfd);
        fprintf(f_write_run, "Request failed: %s\n", get_accept_str(start.Accept));
        fclose(f_write);
        fclose(f_write_run);
        return -2;
    }
    fprintf(f_write_run, "Received ServerStart.\n");

    /* After the TWAMP-Control connection has been established, the
     * Control-Client will negociate and set up some TWAMP-Test sessions */
    struct twamp_test_info *twamp_test = malloc(test_sessions_no * sizeof(struct twamp_test_info));
    if (!twamp_test) {
        fprintf(f_write_run, "Error on malloc\n");
        close(servfd);
        fclose(f_write);
        fclose(f_write_run);
        return -2;
    }

    uint16_t i;
    active_sessions = 0;
    /* Set TWAMP-Test sessions */
    for (i = 0; i < test_sessions_no; i++) {

        /* Setup test socket */
        twamp_test[active_sessions].testfd = socket(AF_INET, SOCK_DGRAM, 0);
        if (twamp_test[active_sessions].testfd < 0) {
            perror("Error opening socket");
            fprintf(f_write_run, "Error opening socket\n");
            continue;
        }

        struct sockaddr_in local_addr;
        memset(&local_addr, 0, sizeof(local_addr));
        local_addr.sin_family = AF_INET;
        local_addr.sin_addr.s_addr = INADDR_ANY;

        /* Try to bind on an available port */
        int check_time = CHECK_TIMES;
        while (check_time--) {
            twamp_test[active_sessions].testport = port_send + rand() % 1000;
            local_addr.sin_port = ntohs(twamp_test[active_sessions].testport);
            if (!bind(twamp_test[active_sessions].testfd, (struct sockaddr *)&local_addr,
                      sizeof(struct sockaddr)))
                break;
        }
        if (check_time < 0) {
            fprintf(f_write_run, "Couldn't find a port to bind to for session %d\n  %s", i + 1, strerror(errno));
            continue;
        }

        fprintf(f_write_run, "Sending RequestTWSession for port %d...\n", twamp_test[active_sessions].testport);
        RequestSession req;
        memset(&req, 0, sizeof(req));
        req.Type = kRequestTWSession;
        req.IPVN = 4; // IP version number
        req.SenderPort = ntohs(twamp_test[active_sessions].testport);
        req.ReceiverPort = ntohs(port_recv + rand() % 1000);
        req.PaddingLength = 27;     // As defined in RFC 6038#4.2
        TWAMPTimestamp timestamp = get_timestamp();
        timestamp.integer = htonl(ntohl(timestamp.integer) + 10);   // 10 seconds for start time
        req.StartTime = timestamp;
        struct timeval timeout;
        timeout.tv_sec = TIMEOUT;
        timeout.tv_usec = 0;
        timeval_to_timestamp(&timeout, &req.Timeout);

        /* Trying to send the RequestTWSession request for this TWAMP-Test */
        rv = send(servfd, &req, sizeof(req), 0);
        if (rv <= 0) {
            fprintf(stderr, "[%d] ", twamp_test[active_sessions].testport);
            perror("Error sending RequestTWSession message");
            fprintf(f_write_run, "Error sending RequestTWSession message");
            close(twamp_test[active_sessions].testfd);
            free(twamp_test);
            close(servfd);
            fclose(f_write);
            fclose(f_write_run);
            return -2;
        }

        /* See the Server's response */
        AcceptSession acc;
        memset(&acc, 0, sizeof(acc));
        rv = recv(servfd, &acc, sizeof(acc), 0);
        if (rv <= 0) {
            fprintf(stderr, "[%d] ", twamp_test[active_sessions].testport);
            perror("Error receiving Accept Session");
            fprintf(f_write_run, "Error receiving Accept Session");
            close(twamp_test[active_sessions].testfd);
            free(twamp_test);
            close(servfd);
            fclose(f_write);
            fclose(f_write_run);
            return -2;
        }
        /* See the Server response to this RequestTWSession message */
        if (acc.Accept != kOK) {
            close(twamp_test[active_sessions].testfd);
            continue;
        }
        twamp_test[active_sessions].port = acc.Port;
        active_sessions++;
        //printf("Port 1: %d %d\n", twamp_test[i].testport, twamp_test[i].port);

    }

    if (active_sessions) {
        fprintf(f_write_run, "Sending StartSessions for all active ports ...\n");
        /* If there are any accepted Test-Sessions then send
         * the StartSessions message */
        rv = send_start_sessions(servfd);
        if (rv <= 0) {
            perror("Error sending StartSessions");
            fprintf(f_write_run, "Error sending StartSessions");
            /* Close all TWAMP-Test sockets */
            for (i = 0; i < active_sessions; i++)
                close(twamp_test[i].testfd);
            free(twamp_test);
            close(servfd);
            fclose(f_write);
            fclose(f_write_run);
            return -2;
        }
    }

    if (id == 1) sleep(5);

    /* For each accepted TWAMP-Test session send test_sessions_msg
     * TWAMP-Test packets */
    for (i = 0; i < active_sessions; i++) {
        uint32_t j;
        for (j = 0; j < test_sessions_msg; j++) {
            SenderUPacket pack;
            memset(&pack, 0, sizeof(pack));
            pack.seq_number = htonl(i * test_sessions_msg + j);
            pack.time = get_timestamp();// chuyen thoi gian unix ->ntp time
            pack.error_estimate = 0x100;//16^2     // Multiplier = 1.

            fprintf(f_write_run, "Sending TWAMP-Test message %d for port %d...\n", j + 1, ntohs(twamp_test[i].port));

            serv_addr.sin_port = twamp_test[i].port;
            rv = sendto(twamp_test[i].testfd, &pack, sizeof(pack), 0,
                        (struct sockaddr *)&serv_addr, sizeof(serv_addr));
            if (rv <= 0) {
                perror("Error sending test packet");
                fprintf(f_write_run, "Error sending test packet");
                continue;
            }

            socklen_t len = sizeof(serv_addr);
            ReflectorUPacket pack_reflect;
            memset(&pack_reflect, 0, sizeof(pack_reflect));
            rv = recvfrom(twamp_test[i].testfd, &pack_reflect, sizeof(pack_reflect), 0,
                          (struct sockaddr *)&serv_addr, &len);

            if (rv <= 0) {
                perror("Error receiving test reply");
                fprintf(f_write_run, "Error receiving test reply");
                continue;
            }

            fprintf(f_write_run, "Received TWAMP-Test message response %d for port %d.\n", j + 1, ntohs(twamp_test[i].port));
            /* Print the round-trip metrics */
            fprintf(f_write, "Sender_port: %d\n", twamp_test[i].testport);
            fprintf(f_write, "Reflector_port: %d\n", ntohs(twamp_test[i].port));
            print_metrics(f_write,j + 1, ntohs(twamp_test[i].port), &pack_reflect);
        }
    }


    /* After all TWAMP-Test packets were sent, send a StopSessions
     * packet and finish */
    if (active_sessions) {
        fprintf(f_write_run, "Sending StopSessions for all active ports ...\n");
        rv = send_stop_session(servfd, kOK, 1);
        if (rv <= 0) {
            perror("Error sending stop session");
            fprintf(f_write_run, "Error sending stop session");
            /* Close all TWAMP-Test sockets */
            for (i = 0; i < active_sessions; i++)
                close(twamp_test[i].testfd);
            free(twamp_test);
            close(servfd);
            fclose(f_write);
            fclose(f_write_run);
            return -2;
        }
    }

    /* Close all TWAMP-Test sockets */
    for (i = 0; i < active_sessions; i++)
        close(twamp_test[i].testfd);
    free(twamp_test);
    close(servfd);
    fclose(f_write);
    fclose(f_write_run);
    return 1;
}
